import { createContext, ReactNode, useContext, useState } from 'react';

type ModalProviderProps = {
  children: ReactNode,
}

type ModalContextData = {
  modalState: boolean,
  openModal: () => void,
  closeModal: () => void
}

export const ModalContext = createContext({} as ModalContextData);

export function ModalProvider ({ children } : ModalProviderProps){
  const [modalState, setState] = useState(false);
  
  function openModal() {
    setState(true);
  }
   
  function closeModal() {
    setState(false);
  }

  return(
    <ModalContext.Provider value={{openModal, closeModal, modalState}}> 
      {children}
    </ModalContext.Provider>
  );
};

export const useModalContext = () => {
  const context = useContext(ModalContext);
  return context;
}