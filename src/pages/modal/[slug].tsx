import { GetStaticPaths, GetStaticProps } from "next";
import Image from "next/image";
import Link from "next/link";
import { useContext } from "react";
import { ModalContext } from "../../context/ModalContext";
import { api, key } from "../../services/api";

import styles from './modal.module.scss';

type Image = {
  id: string;
  author: string;
  title: string;
  height: number;
  width: number;
  url: string;
  downloadUrl: string;
}

type ImageProps = {
  image: Image;
}

export default function Modal({ image } : ImageProps) {
  const { modalState, closeModal } = useContext(ModalContext);

  return (
    <div className={styles.ModalBody}>
      {modalState && (
        <div className={styles.ModalContainer}>
          <Link href="/">
            <button onClick={closeModal} className={styles.Close}>
              <a>X</a>
            </button>
          </Link>
          <Image
            src={image.url}
            alt={image.title}
            width={1080}
            height={560}
            objectFit="cover"
          />
          <h2>
            Title: {image.title ? 
            <span>{image.title}</span> 
            : 
            <h3>no title</h3>}
          </h2>
          <h2>
            Author: <span>{image.author}</span>
          </h2>
          <p>
            Dimensions: {image.width} L x {image.height} A
          </p>
          <button type="button" className={styles.Download}>
            <a href={image.downloadUrl} download target="_blank" rel="noopener">
              Download
            </a>
          </button>
        </div>
      )}
    </div>
  );
}

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await api.get(`/photos/${key}?page=1&per_page=30`);

  const paths = data.map(image => {
    return {
      params: {
        slug: image.id
      }
    }
  })

  return {
    paths,
    fallback: 'blocking'
  }
}

export const getStaticProps: GetStaticProps = async (ctx) => {
  const { slug } = ctx.params;

  const { data } = await api.get(`/photos/${slug}${key}`);

    const image = {
      id: data.id,
      url: data.urls.full,
      author: data.user.name,
      title: data.alt_description,
      height: data.height,
      width: data.width,
      downloadUrl: data.links.html
    }


  return {
    props:{
      image,
    },
    revalidate: 60 * 60 * 8,
  }
}