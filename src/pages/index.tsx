import { GetStaticProps } from 'next'
import { api, key } from '../services/api';
import { useContext } from 'react';
import { ModalContext } from '../context/ModalContext';

import Image from 'next/image';
import Link from 'next/link'

import styles from './home.module.scss';


type Images = {
  id: string,
  urls : string,
  likes: number
}

type HomeProps = {
  images: Images[],
}

export default function Home( { images }: HomeProps) {
  const { openModal, modalState } = useContext(ModalContext);

  return(
    <div className={styles.HomePage}>
    { !modalState &&     
        <section className={styles.allImages}>
          {images.map(image => {
            return(
              <div className={styles.ContainerImage} key={image.id}>
                <Link href={{
                  pathname: `/modal/[slug]`,
                  query: { slug : image.id },
                }}>
                  <a>
                    <Image 
                    width={300} 
                    height={300} 
                    src={image.urls} 
                    alt={image.id} 
                    objectFit="cover"
                    onClick={openModal}
                    />
                  </a>
                </Link>
                <p> likes: {image.likes}</p>
              </div>
            );
          })}
        </section>}
    </div>
  ) ;
}


export const getStaticProps: GetStaticProps = async () => {
  const { data } = await api.get(
    `/photos/${key}?page=1&per_page=30`,
    {
  })

  const images = data.map(image => {
    return{
      id: image.id,
      urls: image.urls.small,
      likes: image.likes
    }
  })
  console.log(images);

  return {
    props:{
      images,
    },
    revalidate: 60 * 60 * 8,
  }
}
