import { Header } from '../components/Header/Header';
import { ModalProvider } from '../context/ModalContext';
import '../styles/global.scss';

function MyApp({ Component, pageProps }) {
  return (
    <ModalProvider >
      <div>
        <Header />
        <Component {...pageProps} />
      </div>
    </ModalProvider>
  );
}

export default MyApp;
