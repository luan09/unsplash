import format from 'date-fns/format';
import Us from 'date-fns/locale/en-US';
import Link from 'next/link';
import styles from './styles.module.scss';

export function Header(){
  const currentDate = format(new Date(), 'EEEEEE, d MMMM',{
    locale: Us,
  })

  return(
    <header className={styles.HeaderContainer}>
      <Link href="https://unsplash.com/" >
        <a target="_blank" rel="noopener">
          <img src="/logo.png" />
        </a>
      </Link>

      <p>Hello, welcome!</p>
      <span>{currentDate}</span>
    </header>
  );
}