module.exports = {
  images: {
    domains: ['images.unsplash.com'],
  },
  future: {
    webpack5: true,
  },
  webpack: function (config, options) {
    config.experiments = {};
    return config;
  },
};